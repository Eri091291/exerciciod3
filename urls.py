from django.urls import path
from .views import viewPrincipal, viewDetalhe, viewDelete, viewCriar, viewAlterar

urlpatterns = [ 
    path("principal/", viewPrincipal, name = "principal"),
    path("postagem/<int:id>/", viewDetalhe, name = "detalhe"),
    path("deletar/<int:id>/", viewDelete, name = "delete"),
    path("criar-postagem/", viewCriar, name ="criar"),
    path("alterar/<int:id>/", viewAlterar, name = "alterar"),
]
